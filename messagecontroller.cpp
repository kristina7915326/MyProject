#include "messagecontroller.h"

msgObject::msgObject(const QString &msg, const bool &from)
    :   ob_msg(msg),
        ob_from(from)
{

}


QString msgObject::msg() const {
    return ob_msg;
}

bool msgObject::from() const {
    return ob_from;
}





msgModel::msgModel(QObject *parent)
{

}

void msgModel::addMsg(const msgObject &newMsg)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    ob_msgs << newMsg;
    endInsertRows();
}


int msgModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ob_msgs.count();

}

QVariant msgModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= ob_msgs.count())
            return QVariant();
    const msgObject &itemToReturn = ob_msgs[index.row()];
    if (role == msgRole)
        return itemToReturn.msg();
    else if (role == fromRole)
        return itemToReturn.from();


    return QVariant();


}

QHash<int, QByteArray> msgModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[msgRole] = "msg";
    roles[fromRole] = "from";
    return roles;
}




messagecontroller::messagecontroller(QObject *parent) : QObject(parent)
{
    serv = new QTcpServer();
    client_sock = new QTcpSocket();

    QObject::connect(serv, SIGNAL(newConnection()),
                    this, SLOT(newConnection()));

    serv->listen(QHostAddress("127.0.0.1"), 33333);
    QObject::connect(client_sock, SIGNAL(readyRead()),
                     this, SLOT(read_client_Message()));
    client_sock->connectToHost("127.0.0.1",33333);
    qDebug() << "Server+Client init" << "\n\n";
}


void messagecontroller::newConnection()
{
    server_sock = serv->nextPendingConnection();
    connect(server_sock, SIGNAL(readyRead()),
                     this, SLOT(read_serv_Message()));
}

void messagecontroller::sendMessage(QString message) {
    QByteArray data;
    data.append(message);
    server_sock->write(data);
    server_sock->waitForBytesWritten();
    client_sock->write(data);
    client_sock->waitForBytesWritten();
}

void messagecontroller::read_client_Message()
{
    QString msg = client_sock->readAll();
    messages.addMsg(msgObject(msg, 1));
}

void messagecontroller::read_serv_Message()
{
    QString msg = server_sock->readAll();
    messages.addMsg(msgObject(msg, 0));
}
