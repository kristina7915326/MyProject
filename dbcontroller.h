#ifndef DBCONTROLLER_H
#define DBCONTROLLER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QAbstractTableModel>
#include "cryptocontroller.h"

class FriendsObject2 {
    public:
        FriendsObject2 (const QString &FriendId,
                       const QString &FriendName,
                       const QString &Photo,
                       const QString &Status);

        QString FriendId() const;
        QString FriendName() const;
        QString Photo() const;
        QString Status() const;
    private:
        QString ob_friendid;
        QString ob_friendname;
        QString ob_photo;
        QString ob_status;
};


class FriendsModel2 : public QAbstractListModel {
    Q_OBJECT
public:
    enum DataRoles {
        FriendIdRole,
        FriendNameRole,
        PhotoRole,
        StatusRole
    };

    FriendsModel2(QObject *parent = 0);

    void addFriend(const FriendsObject2 & newFriend);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    Q_INVOKABLE void updateModel();
    Q_INVOKABLE void clearModel();
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<FriendsObject2> ob_friends;

};


class dbcontroller : public QObject
{
    Q_OBJECT
public:
    Cryptocontroller cryptocontroller;
    FriendsModel2 dbModel;
    explicit dbcontroller(QObject *parent = nullptr);
    QSqlDatabase db;
    QString fileName;
    void initTable(QString filename, QString friends);
    void downModel(QString filename);
    ~dbcontroller();
signals:

public slots:
    void add_friend(QString id, QString name, QString status);
    void delete_friend(QString friend_id);
};

#endif // DBCONTROLLER_H
