import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import QtMultimedia 5.8
import QtGraphicalEffects 1.0

ApplicationWindow {

    signal sendAuth(string login, string password)
    signal sendMsg(string message)
    signal deleteFriend(string friend_id)
    signal add_friend(string id, string name, string status)
    visible: true
    minimumWidth: 640
    minimumHeight: 480
    title: "Кристина Юсупова"
    maximumHeight: minimumHeight
    maximumWidth: minimumWidth
    Rectangle {
        x: 0
        y: 0
        width: parent.width
        height: 40
        color: "darkblue"
        border.color: "gold"
        border.width: 2
        GridLayout {
            anchors.fill: parent
            Rectangle {
                Layout.row: 1
                Layout.column: 1
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: parent.width / 6 - 5
                color: {
                    if (swipeView.currentIndex != 0) "transparent"
                    else "gold"
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "VkAuth"
                    color: {
                        if (swipeView.currentIndex != 0) "gold"
                        else "darkblue"
                    }
                    font.pointSize: 12
                }
                Button {
                    anchors.fill: parent
                    opacity: 0
                    onClicked: swipeView.setCurrentIndex(0)
                }
            }
            Rectangle {
                Layout.row: 1
                Layout.column: 2
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: parent.width / 6 - 5
                color: {
                    if (swipeView.currentIndex != 1) "transparent"
                    else "gold"
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "VkFriends"
                    color: {
                        if (swipeView.currentIndex != 1) "gold"
                        else "darkblue"
                    }
                    font.pointSize: 12
                }
                Button {
                    anchors.fill: parent
                    opacity: 0
                    onClicked: swipeView.setCurrentIndex(1)
                }
            }
            Rectangle {
                Layout.row: 1
                Layout.column: 3
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: parent.width / 6 - 5
                color: {
                    if (swipeView.currentIndex != 2) "transparent"
                    else "gold"
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Web Camera"
                    color: {
                        if (swipeView.currentIndex != 2) "gold"
                        else "darkblue"
                    }
                    font.pointSize: 12
                }
                Button {
                    anchors.fill: parent
                    opacity: 0
                    onClicked: swipeView.setCurrentIndex(2)
                }
            }
            Rectangle {
                Layout.row: 1
                Layout.column: 4
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: parent.width / 6 - 5
                color: {
                    if (swipeView.currentIndex != 3) "transparent"
                    else "gold"
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Video"
                    color: {
                        if (swipeView.currentIndex != 3) "gold"
                        else "darkblue"
                    }
                    font.pointSize: 12
                }
                Button {
                    anchors.fill: parent
                    opacity: 0
                    onClicked: swipeView.setCurrentIndex(3)
                }
            }
            Rectangle {
                Layout.row: 1
                Layout.column: 5
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: parent.width / 6 - 5
                color: {
                    if (swipeView.currentIndex != 4) "transparent"
                    else "gold"
                }

                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Data Base"
                    color: {
                        if (swipeView.currentIndex != 4) "gold"
                        else "darkblue"
                    }
                    font.pointSize: 12
                }
                Button {
                    anchors.fill: parent
                    opacity: 0
                    onClicked: swipeView.setCurrentIndex(4)
                }
            }

            Rectangle {
                Layout.row: 1
                Layout.column: 6
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: parent.width / 6 - 5
                color: {
                    if (swipeView.currentIndex != 5) "transparent"
                    else "gold"
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Chat"
                    color: {
                        if (swipeView.currentIndex != 5) "gold"
                        else "darkblue"
                    }
                    font.pointSize: 12
                }
                Button {
                    anchors.fill: parent
                    opacity: 0
                    onClicked: swipeView.setCurrentIndex(5)
                }
            }
        }
    }
    SwipeView {  // компонент для пролистывания страниц
        id: swipeView
        width: parent.width
        height: 440
        x:0
        y: 40
        currentIndex: tabBar.currentIndex
        Page{
            ColumnLayout {
                clip: false
                opacity: 0.8
                anchors.leftMargin: 0
                anchors.fill: parent;
                Rectangle {
                    Layout.preferredWidth:  70 * Screen.pixelDensity
                    Layout.preferredHeight: 40 * Screen.pixelDensity
                    Layout.alignment: Qt.AlignCenter
                    ColumnLayout {
                        anchors.centerIn: parent;
                        width:50 * Screen.pixelDensity
                        height:50 * Screen.pixelDensity
                        Rectangle {
                            color: "white"
                            border.width: 1
                            border.color: "black"
                            Layout.preferredWidth:  40 * Screen.pixelDensity
                            Layout.preferredHeight: 10 * Screen.pixelDensity
                            Layout.alignment: Qt.AlignCenter
                            TextField {
                                id: edtLogin1
                                font.pointSize: 10
                                anchors.fill: parent;
                                placeholderText: "Enter your login"
                                //при размещении в лэйоут элементов,  мы не используем width и x,y
                            }
                        }
                        Rectangle {
                            color: "white"
                            border.width: 1
                            border.color: "black"
                            Layout.preferredWidth:  40 * Screen.pixelDensity
                            Layout.preferredHeight: 10 * Screen.pixelDensity
                            Layout.alignment: Qt.AlignCenter
                            TextField {
                                id: edtPassword1
                                font.pointSize: 10
                                anchors.fill: parent;
                                echoMode: "Password"
                                placeholderText: "Enter your password"
                                //при размещении в лэйоут элементов,  мы не используем width и x,y
                            }
                        }


                        DropShadow {
                            anchors.fill: btnAuth
                            horizontalOffset: 3
                            verticalOffset: 3
                            radius: 8.0
                            samples: 17
                            color: "#80000000"
                            source: btnAuth
                        }
                        Button {
                            id: btnAuth
                            text: "Вход"
                            font.pointSize: 18
                            Layout.preferredWidth:  40 * Screen.pixelDensity
                            Layout.preferredHeight: 10 * Screen.pixelDensity
                            Layout.alignment: Qt.AlignCenter
                            onClicked: {
                                sendAuth(edtLogin1.text, edtPassword1.text);
                                edtLogin1.clear()
                                edtPassword1.clear()
                                swipeView.setCurrentIndex(swipeView.currentIndex + 1)
                                if (!modelFriends.hasChildren() && dbModel.hasChildren()) {
                                    noFriends.text = "Друзья загружены из базы данных"
                                    swipeView.setCurrentIndex(4)
                                }

                                if (dbModel.hasChildren()) dbnoFriends.opacity = 0
                                if (modelFriends.hasChildren()) noFriends.opacity = 0
                                else 1
                            }


                        }

                    }
                }
            }
        }

        Page {
            id: vkFriends
            Rectangle {
                id: backgroundfriends
                anchors.fill: parent
                color: "black"
            }



            Item {
                id: item1
                anchors.fill: parent
                Text {
                    id: noFriends
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 18
                    color: "lightgray"
                    text: "Вы не авторизованы или у вас нет друзей"
                }
                   ListView {
                       anchors.fill: parent
                       model: modelFriends
                       spacing: 20
                       delegate: Rectangle {
                               id: rec1
                               color: "white"
                               height: 120
                               width: parent.width
                               radius: 10
                               anchors.margins: 20
                               opacity: 0.8

                               GridLayout {
                                   anchors.fill: parent
                                   opacity: 1
                                   Image {
                                       id: avatar
                                       source: photo
                                       anchors.right: parent.right
                                       anchors.top: parent.top
                                       anchors.rightMargin: 25
                                       anchors.topMargin: 10
                                       Layout.row: 1
                                       Layout.column: 2
                                       Layout.rowSpan: 4
                                   }

                                   Text {
                                       id: id1
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 1
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "ID friend - " + friendid
                                   }
                                   Text {
                                       id: id2
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 2
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "Name friend: " + friendname
                                   }

                                   Text {
                                       id: id3
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 3
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "Status:"
                                   }
                                   Text {
                                       id: id4
                                       font.pointSize: 8
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 4
                                       Layout.column: 1
                                       text: {
                                           if (status != "") "\"" + status + "\""
                                           else {
                                               font.pointSize = 12
                                               "Нет статуса"
                                               anchors.horizontalCenter = parent.horizontalCenter
                                           }

                                       }

                                   }
                               }
                           }
                   }
            }
        }

        Page{
            id: photoPage

            Camera {
                id: camera
//                videoRecorder.audioEncodingMode: CameraRecorder.AverageBitRateEncoding
//                videoRecorder.audioBitRate: 48000
                videoRecorder.mediaContainer: "mp4"
                videoRecorder.frameRate: 25
            }

            GridLayout {
                anchors.fill: parent
                VideoOutput {
                    enabled: true
                    Layout.column: 1
                    Layout.row: 1
                    Layout.columnSpan: 6
                    Layout.rowSpan: 5
                    Layout.preferredHeight: parent.height * 0.83
                    Layout.preferredWidth: parent.width
                    source: camera
                }

                Layout.preferredWidth:  40 * Screen.pixelDensity
                Layout.preferredHeight: 30 * Screen.pixelDensity
                Layout.alignment: Qt.AlignCenter

                DropShadow {
                    anchors.fill: btnShot
                    horizontalOffset: 3
                    verticalOffset: 3
                    radius: 8.0
                    samples: 17
                    color: "#80000000"
                    source: btnShot
                }

                Button {
                    id: btnShot
                    text: "Сделать\nфото"
                    Layout.column: 3
                    Layout.row: 6
                    Layout.preferredHeight: 12 * Screen.pixelDensity
                    Layout.preferredWidth: 40 * Screen.pixelDensity
                    Layout.bottomMargin: 20
                    onClicked:  {
                        camera.imageCapture.captureToLocation(applicationDirPath + "/../webshots")
                    }
                 }
            }
        }


    Page {
        MediaPlayer {
            id: mediaPlayer
            autoLoad: true
            loops: 0
            source: "./Bin/video/sample.avi"
            autoPlay: true
        }
        GridLayout {
            anchors.fill: parent
            VideoOutput{
                enabled: true
                Layout.column: 1
                Layout.row: 1
                Layout.columnSpan: 6
                Layout.rowSpan: 5
                Layout.preferredHeight: parent.height * 0.83
                Layout.preferredWidth: parent.width
                source: mediaPlayer
            }

            Text {
                horizontalAlignment: Text.AlignRight
                Layout.preferredWidth: parent.width / 3
                font.pointSize: 16
                Layout.column: 1
                Layout.columnSpan: 2
                Layout.row: 6
                font.bold: true
                text: "Start play"
                color: "darkgreen"
            }


                Image {
                    id: imgStart
                    source: "./Bin/images/play_pressed.png"
                    Layout.column: 3
                    Layout.row: 6
                    Layout.preferredHeight: 12 * Screen.pixelDensity
                    Layout.preferredWidth: 12 * Screen.pixelDensity
                    Layout.bottomMargin: 5
                    Button {
                        id: btnStart
                        opacity: 0
                        onClicked: {
                            mediaPlayer.play()
                            imgStart.source = "file:///" + applicationDirPath + "/../images/play_pressed.png"
                        }
                        onPressed: {
                            imgStart.source = "file:///" + applicationDirPath + "/../images/play.png"
                        }

                    }
                }



                Image {
                    id: imgPause
                    source: "./Bin/images/pause.png"
                    Layout.column: 4
                    Layout.row: 6
                    Layout.preferredHeight: 12 * Screen.pixelDensity
                    Layout.preferredWidth: 12 * Screen.pixelDensity
                    Layout.bottomMargin: 5
                    Button {
                        id: btnPause
                        opacity: 0
                        anchors.fill: parent
                        onClicked: {
                            mediaPlayer.pause()
                            imgPause.source = "file:///" + applicationDirPath + "/../images/pause.png"
                        }
                        onPressed: {
                            imgPause.source = "file:///" + applicationDirPath + "/../images/pause_pressed.png"
                        }
                }
            }

            Text {
                horizontalAlignment: Text.AlignLeft
                Layout.preferredWidth: parent.width / 3
                font.pointSize: 16
                Layout.column: 5
                Layout.columnSpan: 2
                Layout.row: 6
                font.bold: true
                text: "Pause video"
                color: "darkred"
            }
        }

    }

    Page {
        id: dataBase
        Rectangle {
            id: dbbackgroundfriends
            anchors.fill: parent
            color: "black"
        }
        Item {
            id: addFriends

            height: 120
            width: parent.width

            Rectangle {
                id: addFriendsrec
                anchors.fill: parent
                anchors.margins: 5
                opacity: 0.7
                GridLayout {
                    anchors.fill: parent
                    anchors.margins: 5
                    TextField {
                        id: addId
                        Layout.row: 1
                        Layout.column: 1
                        Layout.columnSpan: 2
                        Layout.preferredWidth: parent.width / 2 - 5
                        placeholderText: "Введите ID"
                    }

                    TextField {
                        id: addName
                        Layout.row: 2
                        Layout.column: 1
                        Layout.columnSpan: 2
                        Layout.preferredWidth: parent.width / 2 - 5
                        placeholderText: "Введите Имя"
                    }
                    TextField {
                        id: addStatus
                        Layout.row: 1
                        Layout.column: 3
                        Layout.columnSpan: 2
                        Layout.preferredWidth: parent.width / 2 - 5
                        placeholderText: "Введите Статус"
                    }
                    Button {
                        Layout.row: 2
                        Layout.column: 4
                        text: "Добавить друга"
                        onClicked:  {
                            add_friend(addId.text, addName.text, addStatus.text);
                        }
                    }

                }
            }
        }


        Item {
            id: dbitem1
            anchors.top: addFriends.bottom
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            Text {
                id: dbnoFriends
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 18
                color: "lightgray"
                text: "Вы не авторизованы или у вас нет друзей"
            }


               ListView {
                   anchors.fill: parent
                   model: dbModel
                   spacing: 20
                   delegate: Rectangle {
                           id: dbrec1
                           color: "white"
                           height: 120
                           width: parent.width
                           radius: 10
                           anchors.margins: 20
                           opacity: 0.8

                           GridLayout {
                               anchors.fill: parent
                               opacity: 1
                               Image {
                                   id: avatardb
                                   source: photo
                                   anchors.rightMargin: 25
                                   anchors.topMargin: 10
                                   Layout.row: 1
                                   Layout.column: 2
                                   Layout.rowSpan: 4
                               }


                               Image {
                                   id: deleteImg
                                   source: "Bin/images/delete.png"
                                   anchors.right: parent.right
                                   anchors.top: parent.top
                                   anchors.rightMargin: 25
                                   anchors.topMargin: 10
                                   Layout.maximumHeight: 50
                                   Layout.maximumWidth: 50
                                   Layout.row: 3
                                   Layout.column: 3
                                   Layout.rowSpan: 2
                                   Button {
                                       opacity: 0
                                       anchors.fill: parent
                                       onClicked:  {
                                           deleteFriend(friendid)
                                       }
                                   }
                               }

                               Text {
                                   id: db1
                                   font.pointSize: 12
                                   color: "black"
                                   anchors.left: parent.left
                                   anchors.leftMargin: 10
                                   Layout.row: 1
                                   Layout.column: 1
                                   text: "ID friend - " + friendid
                               }
                               Text {
                                   id: db2
                                   font.pointSize: 12
                                   color: "black"
                                   anchors.left: parent.left
                                   anchors.leftMargin: 10
                                   Layout.row: 2
                                   Layout.column: 1
                                   text: "Name friend: " + friendname
                               }

                               Text {
                                   id: db3
                                   font.pointSize: 12
                                   color: "black"
                                   anchors.left: parent.left
                                   anchors.leftMargin: 10
                                   Layout.row: 3
                                   Layout.column: 1
                                   text: "Status:"
                               }
                               Text {
                                   id: db4
                                   font.pointSize: 8
                                   color: "black"
                                   anchors.left: parent.left
                                   anchors.leftMargin: 10
                                   Layout.row: 4
                                   Layout.column: 1
                                   Layout.maximumWidth: parent.width / 2
                                   text: {
                                       if (status != "") "\"" + status + "\""
                                       else {
                                           font.pointSize = 12
                                           text = "Нет статуса"
                                       }

                                   }

                               }
                           }
                       }
               }
        }
    }


    Page {
        id: chat
        ListView {
            width: parent.width
            height: 400
            model: modelMsgs
            spacing: 20
            delegate: Rectangle {

                    color:{
                        if (from == 0) "black"
                        else "lightgreen"
                        }
                    height: 80
                    width: parent.width / 2
                    anchors.left: {
                        if (from == 0) parent.left
                    }
                    anchors.right: {
                        if (from == 1) parent.right
                    }

                    radius: 10
                    anchors.margins: 20
                    opacity: 0.8

                    Text {

                        text: msg
                        font.pointSize: 12
                        color: {
                            if (from == 0) "white"
                            else "black"
                            }
                    }
            }
        }

        GridLayout {
            x: 0
            y: 380
            height: 60
            width: parent.width



            Rectangle {
                id: messageText
                Layout.preferredWidth: 440
                Layout.preferredHeight: parent.height
                border.color: "lightblue"
                border.width: 2
                Layout.rightMargin: -6

                Flickable
                {
                    id: flick
                    anchors.fill: parent
                    contentWidth: edit.contentWidth
                    contentHeight: edit.contentHeight
                    interactive: true
                    clip: true

                    function ensureVisible(r) {
                        if (contentX >= r.x)
                            contentX = r.x;
                        else if (contentX+width <= r.x+r.width)
                            contentX = r.x+r.width-width;
                        if (contentY >= r.y)
                            contentY = r.y;
                        else if (contentY+height <= r.y+r.height)
                            contentY = r.y+r.height-height;
                    }

                    TextEdit
                    {
                        id: edit
                        width: flick.width
                        height: flick.height
                        focus: true
                        wrapMode: TextEdit.WordWrap
                        selectByMouse: true
                        font.pointSize: 12
                        padding: 10
                        font.family: "TimesNewRoman"
                        onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                    }
                }

                ScrollBar {
                          id: vbar
                          hoverEnabled: true
                          active: true
                          orientation: Qt.Vertical
                          position: edit.cursorRectangle.y / edit.contentHeight
                          //size: edit.height / messageText.contentHeight
                          anchors.top: parent.top
                          anchors.right: parent.right
                          anchors.bottom: parent.bottom
                      }
            }



            Button {
                Layout.preferredWidth: 200
                Layout.preferredHeight: 60
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Отправить"
                    font.pointSize: 12
                    font.italic: true
                }
                onClicked: {
                    if (edit.text != "") sendMsg(edit.text)
                }
            }

        }
    }
}
}
