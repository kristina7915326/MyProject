#ifndef MESSAGECONTROLLER_H
#define MESSAGECONTROLLER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QFile>
#include <QDataStream>
#include <QStringList>
#include <QAbstractListModel>

class msgObject {
    public:
        msgObject (const QString &msg,
                       const bool &from);


        QString msg() const;
        bool from() const;
    private:
        QString ob_msg;
        bool ob_from;
};


class msgModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum DataRoles {
        msgRole,
        fromRole
    };

    msgModel(QObject *parent = 0);

    void addMsg(const msgObject & newFriend);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<msgObject> ob_msgs;

};


class messagecontroller : public QObject
{
    Q_OBJECT
public:
    explicit messagecontroller(QObject *parent = nullptr);
    QTcpServer * serv;
    QTcpSocket * client_sock; // сокет имитирующий соединение
    QTcpSocket * server_sock;
    msgModel messages;
signals:

public slots:

    void newConnection();
    void sendMessage(QString message);
    void read_client_Message();
    void read_serv_Message();
};

#endif // MESSAGECONTROLLER_H
