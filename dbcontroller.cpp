#include "db_controller.h"
#include <QSqlDatabase>
#include <QFile>
#include <QSqlQuery>// класс инкапсулирующий : (инкапсулирует интферфейс для связи с СУБД)
// строку sql запроса
// сигналы и слоты готовности СУБД
// результат, возвращаемый СУБД
// код ошибок
// прочий второстепенный функционал

FriendsObject2::FriendsObject2(const QString &FriendId, const QString &FriendName, const QString &Photo, const QString &Status)
    :   ob_friendid(FriendId),
        ob_friendname(FriendName),
        ob_photo(Photo),
        ob_status(Status)
{

}


QString FriendsObject2::FriendId() const {
    return ob_friendid;
}

QString FriendsObject2::FriendName() const {
    return ob_friendname;
}

QString FriendsObject2::Photo() const {
    return ob_photo;
}

QString FriendsObject2::Status() const {
    return ob_status;
}



FriendsModel2::FriendsModel2(QObject *parent)
{

}

void FriendsModel2::addFriend(const FriendsObject2 &newFriend)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    ob_friends << newFriend;
    endInsertRows();
}


int FriendsModel2::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ob_friends.count();

}

void FriendsModel2::updateModel()
{
    beginRemoveRows(QModelIndex(), 0, rowCount());
    endRemoveRows();

}

void FriendsModel2::clearModel()
{

    beginRemoveRows(QModelIndex(), 0, rowCount());

    endRemoveRows();
    ob_friends.clear();
}

QVariant FriendsModel2::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= ob_friends.count())
            return QVariant();
    const FriendsObject2 &itemToReturn = ob_friends[index.row()];
    if (role == FriendIdRole)
        return itemToReturn.FriendId();
    else if (role == FriendNameRole)
        return itemToReturn.FriendName();
    else if (role == PhotoRole)
        return itemToReturn.Photo();
    else if (role == StatusRole)
        return itemToReturn.Status();

    return QVariant();


}

QHash<int, QByteArray> FriendsModel2::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[FriendIdRole] = "friendid";
    roles[FriendNameRole] = "friendname";
    roles[PhotoRole] = "photo";
    roles[StatusRole] = "status";
    return roles;

}



dbcontroller::dbcontroller(QObject *parent) : QObject(parent)
{

    db = QSqlDatabase::addDatabase("QSQLITE"); // тип бд

}

void dbcontroller::delete_friend(QString friend_id)
{
    db.setDatabaseName("currentDatBase.sqlite"); // имя файла бд к которому будет привязан этот объект БД, привязка к текущему пользователю

    db.open();
    QSqlQuery query("DELETE FROM friends WHERE userID = ?");
    query.bindValue(0, friend_id);
    query.exec();
    query.clear();

    dbModel.clearModel();

    query.prepare("SELECT * FROM friends");
    query.exec();
    QSqlRecord rec;
    rec = query.record();

    while (query.next()) {
        dbModel.addFriend(FriendsObject2(query.value(rec.indexOf("userID")).toString(),
                                         query.value(rec.indexOf("userName")).toString(),
                                         query.value(rec.indexOf("userPhoto")).toString(),
                                         query.value(rec.indexOf("userStatus")).toString()));

    }
    db.close();
}



void dbcontroller::initTable(QString filename, QString friends)
{
    fileName = filename;
    QFile file("currentDatBase.sqlite");
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file.close();
    db.setDatabaseName("currentDatBase.sqlite"); // имя файла бд к которому будет привязан этот объект БД, привязка к текущему пользователю

    db.open();
    QSqlQuery query("CREATE TABLE friends"
                    "(userID varchar(255) ,"
                    "userName varchar(255) ,"
                    "userPhoto varchar(255) ,"
                    "userStatus varchar(255));");
    query.exec();

    int pos1, pos2;
    pos1 = 0;
    pos2 = 0;

    QString friends_ids, name, surname, friends_names, friends_photos, friends_status;
    while (friends.indexOf("\"id\"", pos2) != -1) {
        pos1 = friends.indexOf("\"id\"", pos2) + 5;
        pos2 = friends.indexOf(",", pos1);
        friends_ids = (friends.mid(pos1, pos2 - pos1));

        pos1 = friends.indexOf("\"first_name\"", pos2) + 14;
        pos2 = friends.indexOf("\"", pos1);
        name = friends.mid(pos1, pos2 - pos1);

        pos1 = friends.indexOf("\"last_name\"", pos2) + 13;
        pos2 = friends.indexOf("\"", pos1);
        surname = friends.mid(pos1, pos2 - pos1);

        friends_names = (name + " " + surname);

        pos1 = friends.indexOf("\"photo_100\"", pos2) + 13;
        pos2 = friends.indexOf("\"", pos1);
        friends_photos = (friends.mid(pos1, pos2 - pos1).replace("\\", ""));

        pos1 = friends.indexOf("\"status\"", pos2) + 10;
        pos2 = friends.indexOf("\"", pos1);
        friends_status = (friends.mid(pos1, pos2 - pos1));

        query.prepare("INSERT INTO friends (userID, userName, userPhoto, userStatus)"
                      "VALUES (?, ?, ?, ?)");
        query.bindValue(0, friends_ids);
        query.bindValue(1, friends_names);
        query.bindValue(2, friends_photos);
        query.bindValue(3, friends_status);

        query.exec();
        query.clear();
        dbModel.addFriend(FriendsObject2(friends_ids, friends_names, friends_photos, friends_status));
    }

    db.close();
}

void dbcontroller::downModel(QString filename)
{
    fileName = filename;
    cryptocontroller.DencryptFile(filename, "1234567890", "987654321");
    db.setDatabaseName("currentDatBase.sqlite"); // имя файла бд к которому будет привязан этот объект БД

    db.open();
    QSqlQuery query("SELECT * FROM friends");
    query.exec();
    QSqlRecord rec;
    rec = query.record();

    while (query.next()) {
        dbModel.addFriend(FriendsObject2(query.value(rec.indexOf("userID")).toString(),
                                         query.value(rec.indexOf("userName")).toString(),
                                         query.value(rec.indexOf("userPhoto")).toString(),
                                         query.value(rec.indexOf("userStatus")).toString()));

    }
    db.close();

}

void dbcontroller::add_friend(QString id, QString name, QString status)
{
    db.setDatabaseName("currentDatBase.sqlite"); // имя файла бд к которому будет привязан этот объект БД

    db.open();
    QSqlQuery query("INSERT INTO friends (userID, userName, userPhoto, userStatus)"
                    "VALUES (?, ?, ?, ?)");

    query.prepare("INSERT INTO friends (userID, userName, userPhoto, userStatus)"
                  "VALUES (?, ?, ?, ?)");
    query.bindValue(0, id);
    query.bindValue(1, name);
    query.bindValue(2, "noPhoto");
    query.bindValue(3, status);
    qDebug() << "hello!";
    query.exec();
    query.clear();
    dbModel.addFriend(FriendsObject2(id, name, "no_photo", status));
    db.close();
}

dbcontroller::~dbcontroller()
{
    cryptocontroller.encryptFile(fileName, "1234567890", "123456789");
    db.close();
    QFile file("currentDatBase.sqlite");
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file.close();
}


