#include <QQmlApplicationEngine>
#include "authcontroller.h"
#include "cryptocontroller.h"
#include <QQmlContext>
#include <QSplashScreen>
#include <QWidget>
#include <QApplication>
#include <QTimer>
#include <messagecontroller.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    QSplashScreen splashScreen; //заставка
    splashScreen.setPixmap(QPixmap("./images/hello_screen.png"));
    splashScreen.show();

    QEventLoop loop;
    QTimer::singleShot(1000, &loop, &QEventLoop::quit);
    loop.exec();
    QQmlApplicationEngine engine;
    AuthController authcontroller;

    messagecontroller msg;
    QQmlContext * ctxt = engine.rootContext(); //контекст, для передачи информации из c++ в qml


    ctxt->setContextProperty("modelMsgs", &(msg.messages));
    ctxt->setContextProperty("modelFriends", &(authcontroller.friendsModel));
    ctxt->setContextProperty("dbModel", &(authcontroller.datBase.dbModel));
    ctxt->setContextProperty("applicationDirPath", QGuiApplication::applicationDirPath());
    //    Cryptocontroller cryptocontroller;

//  cryptocontroller.encryptFile("f0.txt", "123456789", "123456789");
//  cryptocontroller.DencryptFile("f1.txt", "123456789", "123456789");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml"))); //контекст отправляется в qml
    if (engine.rootObjects().isEmpty())
        return -1;
    splashScreen.close();

    QObject * appWindow = engine.rootObjects().first();

    QObject::connect(appWindow,
                     SIGNAL(sendAuth(QString, QString)), // чей и какой сигнал
                     &authcontroller,
                     SLOT(Authentificate(QString,QString))); // к чьему и какому слоту

    QObject::connect(appWindow,
                     SIGNAL(sendMsg(QString)),
                     &msg,
                     SLOT(sendMessage(QString)));

    QObject::connect(appWindow,
                     SIGNAL(deleteFriend(QString)),
                     &(authcontroller.datBase),
                     SLOT(delete_friend(QString)));

    QObject::connect(appWindow,
                     SIGNAL(add_friend(QString, QString, QString)),
                     &(authcontroller.datBase),
                     SLOT(add_friend(QString, QString, QString)));

    return app.exec();
}
