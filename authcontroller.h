#ifndef AUTHCONTROLLER_H
#define AUTHCONTROLLER_H

#include <QObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QEventLoop>
#include <QStringList>
#include <QAbstractListModel>
#include <QThread>
#include <dbcontroller.h>



class FriendsObject {
    public:
        FriendsObject (const QString &FriendId,
                       const QString &FriendName,
                       const QString &Photo,
                       const QString &Status);

        QString FriendId() const;
        QString FriendName() const;
        QString Photo() const;
        QString Status() const;
    private:
        QString ob_friendid;
        QString ob_friendname;
        QString ob_photo;
        QString ob_status;
};


class FriendsModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum DataRoles {
        FriendIdRole,
        FriendNameRole,
        PhotoRole,
        StatusRole
    };

    FriendsModel(QObject *parent = 0);

    void addFriend(const FriendsObject & newFriend);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<FriendsObject> ob_friends;

};


class AuthController : public QObject
{
Q_OBJECT
public:
dbcontroller datBase;
FriendsModel friendsModel;
QNetworkAccessManager * na_manager= new QNetworkAccessManager(this);
QNetworkReply * reply;
QNetworkRequest request;
QString response;
QString access_token;
QString client_id;
explicit AuthController(QObject *parent = nullptr);
signals:
authsuccess();

public slots:
//параметры такие же, как и в сигнале
void Authentificate(QString login,QString password);
};
#endif // AUTHCONTROLLER_H
